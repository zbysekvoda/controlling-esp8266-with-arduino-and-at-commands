#include "Arduino.h"

class ESP{
    public:
        //functions according to AT documentation - https://cdn.sparkfun.com/datasheets/Wireless/WiFi/Command%20Doc.pdf
        boolean test();
        boolean reset();
        String getFirmwareInfo();
        boolean setOperationMode(byte mode);
        boolean joinNetwork(char *ssid, char *pass);
        String getAvailableNetwors();
        boolean disconnectFromWifi();
        boolean setAccessPoint(char *ssid, char *pass, byte chanel, byte security);
        String getConnectedDevices();
        String getCurrentConnection();
        boolean connectToSocketServer(char *type, char *addr, byte port);
        boolean connectToSocketServer(byte id, char *type, char *addr, byte port);  
        boolean sendDataSize(int length);
        boolean sendDataSize(byte channel, int length);
        boolean sendDataSize();
        boolean closeSocketConnection();
        String getAssignedIp();
        boolean setConnectionType(byte type);
        boolean manageSocketServer(byte mode);
        boolean manageSocketServer(byte mode, byte port);

        boolean sendString(); //my sending function
        boolean receiveString(); //my receive function
};
